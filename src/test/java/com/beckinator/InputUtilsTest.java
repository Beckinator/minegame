package com.beckinator;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.Assert.*;

public class InputUtilsTest {

    @Before
    public void setUp() {
        InputUtils.init();
    }

    @Test
    public void testIsAlgorithmValid() {
        assertTrue(InputUtils.isAlgorithmValid("dft"));
        assertTrue(InputUtils.isAlgorithmValid("bft"));
        assertTrue(InputUtils.isAlgorithmValid("both"));
        assertFalse(InputUtils.isAlgorithmValid("dijkstras"));
        assertFalse(InputUtils.isAlgorithmValid("mst"));
    }

    @Test
    public void testIsFilePathValid() {
        assertTrue(InputUtils.isFilePathValid("input.txt"));
        assertFalse(InputUtils.isFilePathValid("mine.txt"));
        assertFalse(InputUtils.isFilePathValid("123456789"));
    }

    @Test
    public void testAskUserForAlgorithm() throws IOException {
        System.setIn(new ByteArrayInputStream("MineGame".getBytes()));
        InputUtils.init();
        assertEquals("MineGame", InputUtils.askUserForAlgorithm());

        InputUtils.init();
        assertNull(InputUtils.askUserForAlgorithm());
    }

    @Test
    public void testAskUserForFilePath() throws IOException {
        System.setIn(new ByteArrayInputStream("input.txt".getBytes()));
        InputUtils.init();
        assertEquals("input.txt", InputUtils.askUserForAlgorithm());

        InputUtils.init();
        assertNull(InputUtils.askUserForAlgorithm());
    }

    @Test
    public void testFormatFilePathEntry() {
        String plainPath = "/path/to/file.txt";
        String pathWithQuotes = "'/path/to/file.txt'";

        assertEquals(plainPath, InputUtils.formatFilePathEntry(plainPath));
        assertEquals(plainPath, InputUtils.formatFilePathEntry(pathWithQuotes));
    }
}
