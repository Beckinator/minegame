package com.beckinator;

import org.junit.Test;
import java.io.IOException;

import static org.junit.Assert.*;

public class MineGameTest {

    @Test
    public void testBreadthMineGamePlay() throws IOException {
        MineGame breadthGame = new MineGame("bft", "input.txt");
        breadthGame.play();

        assertEquals(3, breadthGame.winners.size());
        assertEquals(3, breadthGame.maxExplosions);
    }

    @Test
    public void testDepthMineGamePlay() throws IOException {
        MineGame depthGame = new MineGame("dft", "input.txt");
        depthGame.play();

        assertEquals(3, depthGame.winners.size());
        assertEquals(3, depthGame.maxExplosions);
    }
}
