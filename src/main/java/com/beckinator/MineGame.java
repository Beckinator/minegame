package com.beckinator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MineGame {
    int maxExplosions;
    Map<Mine, Integer> winners;
    private MineService mineService;
    private List<Mine> mines;
    private String algorithm;
    private long startTime;
    private long endTime;

    MineGame(String algorithm, String filePath) throws IOException {
        winners = new HashMap<>();
        mineService = new MineService(filePath);
        mines = mineService.getMinesFromFile();
        mineService.setAdjacencyMap(mines);
        this.algorithm = algorithm;
    }

    void play() {
        startTime = System.nanoTime();
        int highestNum = 0;
        for (Mine mine : mines) {
            int num = mineService.getCountDetonated(mine, mines, algorithm);
            if (num == highestNum)
                winners.put(mine, num);
            else if (num > highestNum) {
                winners.clear();
                winners.put(mine, num);
                highestNum = num;
            }
            maxExplosions = highestNum;
            mineService.resetMines(mines);
        }
        endTime = System.nanoTime();
        outputResults();
    }



     private void outputResults() {
        System.out.println("\n***********************************************");
        System.out.println(String.format("*\t%s", algorithm.equals("bft") ? "Breadth First Traversal" : "Depth First Traversal"));
        System.out.println("***********************************************");
        System.out.println("* Maximum number of mines you can explode: " + maxExplosions);
        System.out.println("***********************************************");
        System.out.println(String.format("* The %s with the most explosions: ", (winners.size() > 1) ? "mines":"mine"));
        winners.keySet().forEach(k -> System.out.println(String.format("* (%s)", k)));
        System.out.println("***********************************************");
        System.out.println(String.format("* Time: %d ns", endTime - startTime));
        System.out.println("***********************************************\n");
    }

}
