package com.beckinator;

import java.io.IOException;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        try {
            InputUtils.init();
            Map<String, String> params = InputUtils.getUserInput();

            switch (params.get("algorithm").toLowerCase()) {
                case "bft": new MineGame("bft", params.get("filePath")).play();
                    break;
                case "dft": new MineGame("dft", params.get("filePath")).play();
                    break;
                case "both": new MineGame("bft", params.get("filePath")).play();
                    new MineGame("dft", params.get("filePath")).play();
                    break;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            System.exit(0);
        }
    }
}
