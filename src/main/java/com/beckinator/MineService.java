package com.beckinator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class MineService {
    int numVisitedDepth = 1;
    private String filePath;
    private Map<Mine, List<Mine>> adjacencyMap;
    
    MineService(String filePath) {
        this.filePath = filePath;
        this.adjacencyMap = new HashMap<>();
    }

    List<Mine> getMinesFromFile() throws IOException {
        File file = new File(filePath);
        try(Stream<String> lines = Files.lines(file.toPath())) {
            return lines.map(line -> new Mine(line.split(" ")))
                    .collect(Collectors.toList());
        }
    }

    void resetMines(List<Mine> mines) {
        numVisitedDepth = 1;
        mines.forEach(mine -> mine.detonated = false);
    }

    Integer getCountDetonated(Mine start, List<Mine> mines, String algorithm) {
        if (algorithm.equals("bft")) {
            numVisitedDepth = breadthFirstTraversal(start, mines);
        } else if (algorithm.equals("dft")) {
            depthFirstTraversal(start, mines);
        }
        return numVisitedDepth;
    }

    void depthFirstTraversal(Mine start, List<Mine> mines) {
        mines.get(mines.indexOf(start)).detonated = true;
        List<Mine> adjacentList = adjacencyMap.get(start);

        for (Mine mine : adjacentList) {
            if (!mine.detonated) {
                numVisitedDepth++;
                depthFirstTraversal(mine, mines);
            }
        }
    }

    Integer breadthFirstTraversal(Mine start, List<Mine> mines) {
        int countExploded = 0;
        Queue<Mine> q = new LinkedList<>();
        q.add(start);

        while (!q.isEmpty()) {
            Mine curr = q.poll();
            int currIndex = mines.indexOf(curr);
            if (!mines.get(currIndex).detonated) {
                mines.get(currIndex).detonated = true;
                countExploded++;
                q.addAll(adjacencyMap.get(curr));
            }
        }
        return countExploded;
    }

    void setAdjacencyMap(List<Mine> mines) {
        mines.forEach(start ->
                adjacencyMap.put(start,
                        mines.stream()
                                .filter(mine -> !mine.detonated && withinRadius(start.x, start.y, start.r, mine.x, mine.y))
                                .collect(Collectors.toList())
                )
        );
    }

    boolean withinRadius(double x1, double y1, double r, double x2, double y2) {
        return r >= Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    }
}
