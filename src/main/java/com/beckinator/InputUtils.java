package com.beckinator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class InputUtils {
    private static BufferedReader reader;

    static void init() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    static Map<String, String> getUserInput() throws IOException {
        Map<String, String> params = new HashMap<>();
        String algorithm = askUserForAlgorithm();
        if (!isAlgorithmValid(algorithm)) {
            System.out.println("INVALID ENTRY, I'll give you another chance.");
            algorithm = askUserForAlgorithm();
        }
        if (!isAlgorithmValid(algorithm)) {
            System.out.println("Let's just do both!");
            algorithm = "both";
        }
        params.put("algorithm", algorithm.trim());

        System.out.println();
        String filePath = askUserForFilePath();
        if (!isFilePathValid(filePath)) {
            System.out.println("INVALID ENTRY, I'll give you another chance.");
            filePath = askUserForFilePath();
        }
        if (!isFilePathValid(filePath)) {
            System.out.println("Let's just go with input.txt.");
            filePath = "input.txt";
        }
        params.put("filePath", filePath.trim());
        return params;
    }

    static String askUserForAlgorithm() throws IOException {
        System.out.println("***********************************************");
        System.out.println("Enter algorithm (bft, dft, or both to see which\n one is faster): ");
        System.out.println("***********************************************");
        return reader.readLine();
    }

    static boolean isAlgorithmValid(String algorithm) {
        return Arrays.asList("bft","dft","both").contains(algorithm.toLowerCase());
    }

    static String askUserForFilePath() throws IOException {
        System.out.println("***********************************************");
        System.out.println("Enter file path to the mines: ");
        System.out.println("***********************************************");
        return formatFilePathEntry(reader.readLine());
    }

    static boolean isFilePathValid(String path) {
        path = ("".equals(path)) ? path : path.trim();
        return new File(path).exists();
    }

    // The gnome terminal adds '' around the path of a file when drag and drop is used
    // to submit an input file. The ticks must be removed for the path to be valid.
    static String formatFilePathEntry(String path) {
        if (!path.equals("") && path.substring(0,1).equals("'") && path.substring(path.length() - 1).equals("'"))
            path = path.replaceAll("'", "");
        return path;
    }
}
