package com.beckinator;

public class Mine {
    double x;
    double y;
    double r;
    boolean detonated;

    Mine(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
        detonated = false;
    }

    Mine(String[] args) {
        this.x = Integer.parseInt(args[0]);
        this.y = Integer.parseInt(args[1]);
        this.r = Integer.parseInt(args[2]);
        this.detonated = false;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s", this.x, this.y, this.r);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Mine))
            return false;
        Mine b = (Mine) o;
        return (b.x == this.x && b.y == this.y && b.r == this.r);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) this.x;
        hash = 31 * hash + (int) this.y;
        hash = 31 * hash + (int) this.r;
        return hash;
    }
}
