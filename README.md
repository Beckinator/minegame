# The Mine Game

## Premise

Out of all the mines in a mine field, you are tasked with
detonating as many of them as you can. The caveat is you can
only detonate one manually. The mine you manually detonate will potentially set
off a chain reaction of explosions. For any mine that is detonated, all mines within
the blast radius of that mine will also be detonated.

## Input File
The input file is a space delimited .txt file that represents the coordinates of the mines
in the mine field and their blast radius:
(x-coordinate, y-coordinate, radius).

Example:
```bash
1 2 3
1 1 1
2 1 1
6 1 1
```

## Run The Game
SDK versions used: Java 1.8 & Gradle 5.1

After cloning the project, you can open up a terminal window and enter this command or cd into the top level of the project directory.
All commands will be entered while in the top level of the project directory. Next, create a jar file to run the command line interface 
for the game. The following command will create the jar:
```bash
gradle jar
```

To start the game, run this command:
```bash
java -jar ./build/libs/MineGame.jar
```


## Game Play
The player will be prompted for two inputs. The first is the choice of algorithms to solve the problem
the game poses. Of all the mines in the mine field which mine(s) will cause a chain reaction that detonates the 
most mines? Because of the graph-like structure the mine coordinates create, the algorithm choices will be between a 
Depth First Traversal (dft), Breadth First Traversal (bft), or both to compare to see which one runs faster:
```bash
Enter algorithm (bft, dft, or both to see which one is faster): 
```

The second prompt will be for the full path to your text file of mine coordinates. You can type out the path or simply
drag and drop the input file you want to use into the terminal when prompted. To use the text file included in the
project enter "input.txt":
```bash
Enter file path to the mines: 
```
After entering the path to your text file you should see the final results of the game. The maximum number of mines exploded 
and which mine coordinate(s) triggered that many:
```bash
***********************************************
*	Breadth First Traversal
***********************************************
* Maximum number of mines you can explode: 3
***********************************************
* The mine(s) with the most explosions: 
* (2.0, 1.0, 1.0)
* (1.0, 1.0, 1.0)
* (1.0, 2.0, 3.0)
***********************************************
* Time: 3678314 ns
***********************************************


***********************************************
*	Depth First Traversal
***********************************************
* Maximum number of mines you can explode: 3
***********************************************
* The mine(s) with the most explosions: 
* (1.0, 2.0, 3.0)
* (2.0, 1.0, 1.0)
* (1.0, 1.0, 1.0)
***********************************************
* Time: 354050 ns
***********************************************
```

## Tests
To run the tests, while in the top level of the project directory enter this command:
```bash
gradle test
```

## Algorithms
Since the coordinates of the mines in the mine field are known, they create a graph structure.
To figure out which mine(s) cause the the biggest chain reaction you need to traverse the nodes in the 
graph the mine coordinates create. The algorithms that are used to solve this problem in The Mine Game
are Breath First Traversal and Depth First Traversal. The detonation radius of a mine comes into play
when finding all of the adjacent mines, mines within blast radius, for the current mine being evaluated.
The user is given the choice to choose which algorithm to use because some data groups, mine fields,
are solved faster using a BFT and some faster with a DFT.



